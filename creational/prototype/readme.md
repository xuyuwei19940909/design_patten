# checklist
- add clone() method to the hierarchy of our object
- design a registry that maintains a cache of prototypical objects
- design API that allows to clone object inside the factory and return new Object as result
- Use factory API instead of NEW keyword to instantiate the object