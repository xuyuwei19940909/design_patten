# checklist

- define hierarchy of object
- design the arguments for the factory method. 
    - Think about qualities or characteristics that are necessary and sufficient to identify the correct derived class to instantiate
- call factory method during the runtime to instantiate the object that you need