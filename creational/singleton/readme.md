# checklist
- define a private static field in 'singleton' class
- make constructor private
- implement public accessor function
- implement lazy intialzation that is creation on first use
     - inside the accessor function 
- make sure that accessor function is synchronized in case it is going to be used in multitheading environment