# checklist

- define iterator interface with least hasNext() and next() method
- other methods may be added too as we discussed in out example
- in your object that serves as a container declare a method to return an iterator
- create implementation of iterator for collection that you need to iterate over
- client requests iterator and use it for iteration over elements in container