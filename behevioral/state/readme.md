# checklist

- define a hierarchy of state of your domain object
- create implementations and concrete classes of states implementing behavior that should be different based on state of your domain object
- make sure you have API to pass object to state classes (either via constructor , setter)
- make sure that your domain object contains the reference to the most relevant state object
- update state of the domain object when needed
- delegate method calls on domain object to current state object