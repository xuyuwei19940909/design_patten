package state;

public interface State {
	
	void publish(User user);

}