# checklist

- examine the algorithm and think how to split the algorithm into different steps
- create an abstract class and define template method there
- put each step of the algorithm in the separate method
- describe method invocation sequence in template method
- identify places for hooks in your algorithm
- create concrete classes that implements the required steps in the algorithm