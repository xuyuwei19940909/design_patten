package interpreter.demo2.expressions;

import interpreter.demo2.Employee;

public interface Expression {
	
	public boolean interpret(Employee context);
}