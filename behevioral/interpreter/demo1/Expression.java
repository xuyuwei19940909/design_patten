package interpreter.demo1;

public interface Expression {
    String interpret(InterpreterContext ic);
}