# Interpreter 產生的動機
當一個特定的問題發生的頻率夠高，那麼就應該值得定義一個簡單語言來表示它，
而當定義了這樣的語言後，就可以使用 `Interpreter Pattern` 來解決轉譯句子的問題。

- 舉例來說：
    搜尋特定符合特定模式的字串，這是一個很通用常見的問題，
    而 Regular Expression 就是一個用來指定字串模式的標準的語言。
    而要怎麼將一個這樣的語言轉換成程式語言可使用的表示，
    就可以透過套用 Interpreter Pattern，
    將文字敘述轉換成抽象語法樹（Abstract Syntax Tree），就可為程式語言使用。

# checklist

- define a grammar for language that you want to interpret
- map each production in the grammar to class
- organize the suite of grammar classes into the structure of the composite pattern
- define an interpret() method that takes context as a parameter in the composite hierarchy
- the context object encapsulates the current state of the input and output. 
it is affected by each grammar class as the `interpreting` process transforms the input into the output