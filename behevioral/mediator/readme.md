checklist

- identify group of classes that you want to decouple
- create new abstraction and encapsulate all interactions 
    inside a new class - mediator class 
- update all existing classes 
    to make them interact with mediator object only