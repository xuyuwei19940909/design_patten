# checklist

- define the common interface for commands and identify the command enter point
- create implementations of this interface
- each command should store reference to the receiver object
- also, command should store all necessary arguments to pass to receiver
- add command to the requester object - the one that sends the request 
- invoke command's method when needed 
    and pass all necessary arguments to interact receiver


### Strategy VS Command

- The Command pattern is used to make an object out of what needs to be done
- The strategy pattern, on the other hand , is used to specify how something should be done

另一個方面來說，
strategy 是用不同的方式做同一件事
command 是做不同的事，而且會有一個request跟接受者