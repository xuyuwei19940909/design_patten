# checklist

- create interface of event handler and declare method of event handing
- make sure that each implementation of the chain interface may contain the reference to the next link in the chain
- each chain interface implementation contrbutes to the event handing
- if the request is needed to be passed forward, link object calls the method on the next element in the chain
- client creates chain depending on the business logic and launches chain form the root