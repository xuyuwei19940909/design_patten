# checklist

- identify and split roles of originator and caretaker between classes 
- create memento type that contains enough information to restore originator object
- caretaker store information about all states of originator and knows how to save and restore state of originator object
- originator object may create memento object
- originator may re-instantiate itself using saved state in Memento object