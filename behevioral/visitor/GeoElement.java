package visitor;

public interface GeoElement {
    void accept(Visitor visitor);
}