# clicklist

- GeoElement 是要被處理的物件，visitor是策略，
- create visitor interface and methods to visitor each class of the elements hierarchy you need to work with 
- declare and describe interface of Elements
- Implement Element interface in each concrete class
- Hierarchy of elements needs to be aware only about the most abstract type of visitors. 
- on the other hand , hierarchy of visitors should know all concrete implementations of element hierarchy
- for each new behaviot create concrete implementation of Visitor interface
- client will create visitor object and then will pass it to the acceptance method of each concrete element

### Visitor VS Decorator
`Visitor`: works with hierarchy of elements
`Decorator`: works with single object