# checklist
- identify group of algorithms in your app
- create an interface for such group of algorithms
- create concrete classes that implements the interface and concrete algorithms
- and you know that in Java language you can also use lambda expressions or method reference because most likely, your strategy interface will be a functional interface
- Use the strategy in client code wherever it is needed