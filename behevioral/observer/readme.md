# checklist

- separate dependent and independent behavior. 
    - Dependent behavior should be executed in case when some event happens.
- when you separated these types of behavior, make sure you have separate hierarchy for your subject with independent behavior and hierarchy of observers with dependent behavior
- make sure that in Observe interface you have method for notification
- subject may store references to observers
- observers should register themselves in subject object
- subject notifies all observers about event when it happens
- Subject may "push" information to observers, or let observers 'pull' needed information from subject object