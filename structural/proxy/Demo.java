package proxy;

public class Demo {
    public static void main(String[] args) {
        Internet internet = new ProxyInternet();

        internet.connectToHost("is-bulls.com");
        internet.connectToHost("facebook.com");
    }
}