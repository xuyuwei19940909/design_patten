package proxy;

public interface Internet {
    void connectToHost(String url);
}