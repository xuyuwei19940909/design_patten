package proxy;

public class DefaultInternet implements Internet {
    // @override
    public void connectToHost(String url) {
        System.out.println("connecting to " + url);
    }
}