package decorator;

public class SportsCar extends CarDecorator {
    private String transmissMode;

    public SportsCar(Car c) {
        super(c);
    }

    // Override
    public void drive() {
        super.drive();
        System.out.println(" And drives fast aslike a Sports Car");
    }

    public void setTransmissMode(String transmissMode) {
        this.transmissMode = transmissMode;
        System.out.println("transmissMode");
    }
}