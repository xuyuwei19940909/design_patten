package decorator;

public class LuxuryCar extends CarDecorator {
    public LuxuryCar(Car car) {
        super(car);
    }

    public void drive() {
        super.drive();
        System.out.println(" And drives soft as LuxuCar");
    }
}