package decorator;

public class Demo {
    public static void main(String[] args) {
        Car basicCar = new BasicCar();
        basicCar.drive();
        System.out.println("\n*******");

        SportsCar sportsCar = new SportsCar(new BasicCar());
        sportsCar.drive();
        sportsCar.setTransmissMode("8888888");
        System.out.println("\n*******");

        Car sportsLuxuryCar = new SportsCar(new LuxuryCar(new BasicCar()));
        sportsLuxuryCar.drive();
        sportsCar.setTransmissMode("999999");
        System.out.println("\n*******");
    }
}