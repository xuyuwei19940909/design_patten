# checklist

- identify the interface that you need to follow 
- identify type that you need to adapter
- create adapter that implements target interface and aggregates
- create adapter that implements target interface and aggregates type you need to adapt.
- Wrapper class should have the reference to the adapter class
- instantiate adapter in your client code and just use it