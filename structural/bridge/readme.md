# checklist
- define whether you have two hierarchies that can be improved and enhanced independently, if yes -> then split them in two hierarchies of abstractions and implementations.

- think about operations that would be needed to your client describe their operations in abstract class

- if you need new pperactions - create new abstract class
- design the separation of concerns - what is the abstraction and what is the implementation of behavior

- create concrete classes of your domain type and behavior

- add the reference to the implementation to the class of the abstraction

- delegate calls to the target object at a runtime