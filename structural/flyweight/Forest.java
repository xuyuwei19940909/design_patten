package flyweight;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.awt.*;

public class Forest extends JFrame  {
    private List<Tree> trees = new ArrayList<>();

    // main logic
    public void plantTree(int x, int y, String name, Color color, String otherTreeData) {
        TreeType type = TreeTypeFactory.getTreeType(name, color, otherTreeData);
        Tree tree = new Tree(x, y, type);
        this.trees.add(tree);
    }

    @Override
    public void paint(Graphics graphics) {
        for (Tree tree : this.trees) {
            tree.draw(graphics);
        }
    }
}
