# checklist

- split state of our object in two group - inner and outer state
- we should clearly understand what state is the same across all objects and map be shared, and what state is unique

- create a type where constant fields will be initialized through the constructor
- make sure that outer state may be passed as method arguments
- create factory that will cache shared state and will return already created objects.
- client should request object with specific state but not created directly
- client should store outer state, or generate it and pass it as an argument to a flyweight

### attention
for this example Tree.x , Tree.y is unique
Tree.name , Tree.color , Tree.otherTreeData is shared
so name, color, otherTreeData wrap by TreeType