# checklist

- make sure that you can apply tree-like structure for your components
- create unified interface that will combine operations on the group of objects and on the single object
- create class of single object
- create class for group of objects that implements the same unified interface 
- add operations to add and remove components into the container