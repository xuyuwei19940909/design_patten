# checklist

- define business cases and business scenarios that are most used in your app
- design a facade interface that decribes behavior to interact with subsystem
- aggregate all necessary type inside the facade to ensure efficient abnd proper behavior of facede
- in client code get instance of facade and work with it
- facade should not become an object